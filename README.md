## Keepers docs

First and foremost, initialize submodules:
```sh
git submodule update --init --recursive
```

To run this project you need [Hugo (extended)]. Grab latest release, install it, and run
```sh
hugo serve
```

### Docker

Project also can be built into self-contained image with bundled [web server]

```
docker build . -t keepers-wiki
docker run -p 8080:80 keepers-wiki
```

[Hugo (extended)]: https://gohugo.io/getting-started/installing/
[web server]: https://sws.joseluisq.net/
