---
title: "Главная"
exclude_search: true
---

{{< block "grid-2" >}}
{{< column >}}

# Группа «Хранители»

Мы занимаемся поддержкой редких раздач на [rutracker.org](https://rutracker.org).

Благодаря нам возможно скачать любой материал, когда-либо выложенный на трекере.

{{< tip >}}
Вы можете дополнить справку, написав кураторам в Telegram-канале
{{< /tip >}}

{{< button "docs/" "Читать справку" >}}{{< button "https://rutracker.org/forum/viewtopic.php?t=3118460" "Стать хранителем" >}}
{{< /column >}}

{{< column >}}
![](/images/data_center.jpg)
{{< /column >}}
{{< /block >}}
