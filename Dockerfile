FROM klakegg/hugo:ext-alpine-onbuild AS hugo

FROM joseluisq/static-web-server:2
COPY --from=hugo /target /public
